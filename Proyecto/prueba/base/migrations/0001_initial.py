# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-11-13 02:21
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='usuario',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=35)),
                ('apellido', models.CharField(default='', max_length=35)),
                ('correo', models.CharField(max_length=35)),
            ],
        ),
    ]
