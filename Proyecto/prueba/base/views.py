from django.shortcuts import render
from django.views.generic import TemplateView
from .models import usuario


class IndexView(TemplateView):
    template_name = "index.html"

    def get(self, request):
        return render(request, self.template_name,
                      context={'request': request})


