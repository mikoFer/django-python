from django.db import models

class usuario(models.Model): #cualquier cambio en models hacer makemigrations
    nombre = models.CharField(max_length=35)
    apellido = models.CharField(max_length=35, default="")
    correo = models.CharField(max_length=35)

    def nombreCompleto(self):#funcion que concatena el nombre y apallido
        cadena= "{0} {1}"
        return cadena.format(self.nombre,self.apellido)

    def __str__(self):#funcion que retorna nombre completo
        return self.nombreCompleto()
